package posonline;

import java.awt.Toolkit;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

import config.dbconnect;
import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.SQLException;

public class main extends javax.swing.JFrame {
    
    public void checklogin(){
            Connection conn = new dbconnect().getconnection();
        try{
            String checkuserpass = "SELECT user,pass,usergroup FROM user";
            if(("".equals(txtuser.getText())) && ("".equals(txtpass.getText()))){
                JOptionPane.showMessageDialog(this,"กรุณากรอกข้อมูลให้ครบทุกช่องด้วยค่ะ","แจ้งเตือนการกรอกข้อมูล",JOptionPane.WARNING_MESSAGE);
            }
            else if("".equals(txtuser.getText())){
                JOptionPane.showMessageDialog(this,"กรุณากรอกขื่อผู้ใช้ด้วยค่ะ","แจ้งเตือนการกรอกข้อมูล",JOptionPane.WARNING_MESSAGE);
            }
            else if("".equals(txtpass.getText())){
                JOptionPane.showMessageDialog(this,"กรุณากรอกรหัสผ่านด้วยค่ะ","แจ้งเตือนการกรอกข้อมูล",JOptionPane.WARNING_MESSAGE);
            }
            else{
                ResultSet reuserpass = conn.createStatement().executeQuery(checkuserpass);
                boolean checkfalselogin = false;
                OUTER:
                while (reuserpass.next()) {
                    if (txtuser.getText().equals(reuserpass.getString("user")) && txtpass.getText().equals(reuserpass.getString("pass"))) {
                        switch (reuserpass.getString("usergroup")) {
                            case "admin":
                                mainpos mainpos = new mainpos();
                                mainpos.show();
                                checkfalselogin = false;
                                this.dispose();
                                break OUTER;
                            case "cashier":
                                String nameem = "SELECT fname, lname, user FROM employee WHERE employee.user = '"+reuserpass.getString("user")+"'";
                                ResultSet renameem = conn.createStatement().executeQuery(nameem);
                                while(renameem.next()){
                                    cashier cashier = new cashier();
                                    cashier.setnameem(renameem.getString("fname") + " " +renameem.getString("lname"));
                                    System.out.println("ส่ง "+reuserpass.getString("user"));
                                    cashier.show();
                                    checkfalselogin = false;
                                    this.dispose();
                                    break OUTER;
                                }
                                break OUTER;
                        }
                    } else {
                        checkfalselogin = true;
                    } 
                }
                if(checkfalselogin == true){
                    JOptionPane.showMessageDialog(this,"ไม่สามารถเข้าสู่ระบบได้ค่ะ กรุณาตรวจสอบข้อมูลผู้ใช้ให้ถูกต้อง","แจ้งเตือนการเข้าสู่ระบบ",JOptionPane.WARNING_MESSAGE);
                }
            }
        }
        catch(HeadlessException | SQLException e){
            JOptionPane.showMessageDialog(this,e,"ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
        }
    }

    public main() {
        initComponents();
        setIcon();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        luser = new javax.swing.JLabel();
        lpass = new javax.swing.JLabel();
        bsummit = new javax.swing.JButton();
        bcancle = new javax.swing.JButton();
        txtuser = new javax.swing.JTextField();
        txtpass = new javax.swing.JPasswordField();
        bclear = new javax.swing.JButton();
        bsetting = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("กรอกชื่อผู้ใช้และรหัสผ่าน เพื่อเข้าสู่ระบบ");
        setAlwaysOnTop(true);
        setBounds(new java.awt.Rectangle(500, 300, 500, 250));
        setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        setFocusTraversalPolicyProvider(true);
        setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        setIconImages(null);
        setMaximizedBounds(new java.awt.Rectangle(500, 300, 500, 250));
        setMinimumSize(new java.awt.Dimension(500, 250));
        setName("login"); // NOI18N
        setUndecorated(true);
        setType(java.awt.Window.Type.POPUP);

        luser.setFont(new java.awt.Font("JasmineUPC", 1, 36)); // NOI18N
        luser.setText("ชื่อผู้ใช้");
        luser.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        luser.setName(""); // NOI18N
        luser.setPreferredSize(new java.awt.Dimension(50, 50));

        lpass.setFont(new java.awt.Font("JasmineUPC", 1, 36)); // NOI18N
        lpass.setText("รหัสผ่าน");
        lpass.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lpass.setName(""); // NOI18N
        lpass.setPreferredSize(new java.awt.Dimension(50, 50));

        bsummit.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        bsummit.setForeground(new java.awt.Color(255, 255, 255));
        bsummit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Administrator-icon.png"))); // NOI18N
        bsummit.setToolTipText("");
        bsummit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bsummitMouseClicked(evt);
            }
        });
        bsummit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                bsummitKeyPressed(evt);
            }
        });

        bcancle.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        bcancle.setForeground(new java.awt.Color(255, 255, 255));
        bcancle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Button-Close-icon.png"))); // NOI18N
        bcancle.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bcancleMouseClicked(evt);
            }
        });

        txtuser.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        txtuser.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtuserKeyReleased(evt);
            }
        });

        txtpass.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        txtpass.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtpassKeyReleased(evt);
            }
        });

        bclear.setFont(new java.awt.Font("JasmineUPC", 0, 24)); // NOI18N
        bclear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Actions-edit-clear-icon.png"))); // NOI18N
        bclear.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bclearMouseClicked(evt);
            }
        });

        bsetting.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Settings-icon.png"))); // NOI18N
        bsetting.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bsettingMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(lpass, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(bsummit)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bclear)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bcancle)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bsetting, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                            .addComponent(txtpass, javax.swing.GroupLayout.PREFERRED_SIZE, 323, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(luser, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(txtuser, javax.swing.GroupLayout.PREFERRED_SIZE, 323, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(49, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(32, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtuser, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(luser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtpass, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lpass, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(bsummit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(bcancle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(bclear, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(bsetting, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(24, 24, 24))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bsummitKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bsummitKeyPressed
      if(evt.getKeyCode() == 10){
          checklogin();
      } 
    }//GEN-LAST:event_bsummitKeyPressed

    private void txtuserKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtuserKeyReleased
      if(evt.getKeyCode() == 10){
          checklogin();
      } 
    }//GEN-LAST:event_txtuserKeyReleased

    private void txtpassKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtpassKeyReleased
      if(evt.getKeyCode() == 10){
          checklogin();
      }
    }//GEN-LAST:event_txtpassKeyReleased

    private void bsummitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bsummitMouseClicked
          checklogin();
    }//GEN-LAST:event_bsummitMouseClicked

    private void bcancleMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bcancleMouseClicked
        System.exit(0);
    }//GEN-LAST:event_bcancleMouseClicked

    private void bclearMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bclearMouseClicked
        txtuser.setText("");
        txtpass.setText("");
    }//GEN-LAST:event_bclearMouseClicked

    private void bsettingMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bsettingMouseClicked
        settingdb settingdb = new settingdb();
        settingdb.show();
    }//GEN-LAST:event_bsettingMouseClicked

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bcancle;
    private javax.swing.JButton bclear;
    private javax.swing.JButton bsetting;
    private javax.swing.JButton bsummit;
    private javax.swing.JLabel lpass;
    private javax.swing.JLabel luser;
    private javax.swing.JPasswordField txtpass;
    private javax.swing.JTextField txtuser;
    // End of variables declaration//GEN-END:variables

    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/img/Cash-register-icon.png")));
    }
}
