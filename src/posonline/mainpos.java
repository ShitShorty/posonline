package posonline;

import java.awt.Toolkit;

public class mainpos extends javax.swing.JFrame {

    public mainpos() {
        initComponents();
        setIcon();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Stock = new javax.swing.JButton();
        customer = new javax.swing.JButton();
        report = new javax.swing.JButton();
        sale = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("โปรแกรม PosOnline");
        setBounds(new java.awt.Rectangle(250, 10, 300, 650));
        setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        setMaximizedBounds(new java.awt.Rectangle(250, 10, 300, 650));
        setMinimumSize(new java.awt.Dimension(300, 650));
        setModalExclusionType(java.awt.Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
        setName("mainpos"); // NOI18N
        setType(java.awt.Window.Type.POPUP);

        Stock.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        Stock.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Where-Is-It-icon.png"))); // NOI18N
        Stock.setText("Stock สินค้า");
        Stock.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                StockMouseClicked(evt);
            }
        });

        customer.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        customer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/User-Group-icon.png"))); // NOI18N
        customer.setText("พนักงาน");
        customer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                customerMouseClicked(evt);
            }
        });

        report.setFont(new java.awt.Font("JasmineUPC", 1, 22)); // NOI18N
        report.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Clipboard-icon.png"))); // NOI18N
        report.setText("รายงานยอด");

        sale.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        sale.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/sale-icon.png"))); // NOI18N
        sale.setText("ขายสินค้า");
        sale.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                saleMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(Stock, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(customer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(sale, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(report, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(Stock)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(customer)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(sale)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(report)
                .addGap(23, 23, 23))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void StockMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_StockMouseClicked
        pagestock pagestock = new pagestock();
        pagestock.show();
    }//GEN-LAST:event_StockMouseClicked

    private void customerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_customerMouseClicked
        employeemain customermenu = new employeemain();
        customermenu.show();
    }//GEN-LAST:event_customerMouseClicked

    private void saleMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saleMouseClicked
        cashier sell = new cashier();
        sell.show();
    }//GEN-LAST:event_saleMouseClicked

    
    public static void main(String args[]) {
   
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new mainpos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Stock;
    private javax.swing.JButton customer;
    private javax.swing.JButton report;
    private javax.swing.JButton sale;
    // End of variables declaration//GEN-END:variables

    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/img/Cash-register-icon.png")));
    }
}
