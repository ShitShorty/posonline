package posonline;

import java.awt.Toolkit;

public class pagestock extends javax.swing.JFrame {

    public pagestock() {
        initComponents();
        setIcon();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        groupproduct = new javax.swing.JButton();
        addproduct = new javax.swing.JButton();
        checkfrontproduct = new javax.swing.JButton();
        exit = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Stock สินค้า");
        setBounds(new java.awt.Rectangle(550, 10, 616, 238));
        setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        setMaximizedBounds(new java.awt.Rectangle(550, 10, 616, 238));
        setMaximumSize(new java.awt.Dimension(616, 238));
        setMinimumSize(new java.awt.Dimension(616, 238));

        groupproduct.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        groupproduct.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Add-Folder-icon.png"))); // NOI18N
        groupproduct.setText("เพิ่ม/ลด หมวดสินค้า");
        groupproduct.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                groupproductMouseClicked(evt);
            }
        });

        addproduct.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        addproduct.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/shop-cart-add-icon.png"))); // NOI18N
        addproduct.setText("เพิ่ม/ลด สินค้า");
        addproduct.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addproductMouseClicked(evt);
            }
        });

        checkfrontproduct.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        checkfrontproduct.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Actions-view-pim-tasks-icon.png"))); // NOI18N
        checkfrontproduct.setText("เช็คสินค้าบนชั้นวาง");

        exit.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        exit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Apps-session-logout-icon.png"))); // NOI18N
        exit.setText("ออก");
        exit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                exitMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(groupproduct)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(addproduct, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(checkfrontproduct, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(exit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(groupproduct)
                    .addComponent(addproduct))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(checkfrontproduct)
                    .addComponent(exit))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exitMouseClicked
        this.dispose();
    }//GEN-LAST:event_exitMouseClicked

    private void groupproductMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_groupproductMouseClicked
        addgroupproduct addgroup = new addgroupproduct();
        addgroup.show();
        this.dispose();
    }//GEN-LAST:event_groupproductMouseClicked

    private void addproductMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addproductMouseClicked
        addproduct addproductb = new addproduct();
        addproductb.show();
        this.dispose();
    }//GEN-LAST:event_addproductMouseClicked

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new pagestock().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addproduct;
    private javax.swing.JButton checkfrontproduct;
    private javax.swing.JButton exit;
    private javax.swing.JButton groupproduct;
    // End of variables declaration//GEN-END:variables

    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/img/Cash-register-icon.png")));
    }
}
