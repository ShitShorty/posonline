
package posonline;

import config.dbconnect;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class edit_em extends javax.swing.JFrame {
    
    Connection conn = new dbconnect().getconnection();
    DefaultTableModel modellistgroup;
    String username = "";

    public edit_em() {
        initComponents();
        setIcon();
        printem();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        listallem = new javax.swing.JTable();
        ok = new javax.swing.JButton();
        cancle = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("รายชื่อพนักงานทั้งหมด");
        setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        listallem.setAutoCreateRowSorter(true);
        listallem.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        listallem.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ลำดับ", "ชื่อ - นามสกุล", "รหัสพนักงาน", "หน้าที่"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        listallem.setToolTipText("");
        listallem.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        listallem.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        listallem.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
        listallem.setDropMode(javax.swing.DropMode.ON);
        listallem.setRowHeight(30);
        listallem.setSurrendersFocusOnKeystroke(true);
        listallem.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listallemMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(listallem);
        if (listallem.getColumnModel().getColumnCount() > 0) {
            listallem.getColumnModel().getColumn(0).setMinWidth(5);
            listallem.getColumnModel().getColumn(0).setPreferredWidth(5);
            listallem.getColumnModel().getColumn(1).setMinWidth(100);
            listallem.getColumnModel().getColumn(1).setPreferredWidth(100);
            listallem.getColumnModel().getColumn(2).setMinWidth(20);
            listallem.getColumnModel().getColumn(2).setPreferredWidth(20);
            listallem.getColumnModel().getColumn(3).setMinWidth(20);
            listallem.getColumnModel().getColumn(3).setPreferredWidth(20);
        }

        ok.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        ok.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Ok-icon.png"))); // NOI18N
        ok.setText("ตกลง");
        ok.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                okMouseClicked(evt);
            }
        });

        cancle.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        cancle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Actions-exit-icon.png"))); // NOI18N
        cancle.setText("ยกเลิก");
        cancle.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cancleMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 580, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(92, 92, 92)
                        .addComponent(ok)
                        .addGap(86, 86, 86)
                        .addComponent(cancle)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ok)
                    .addComponent(cancle))
                .addGap(0, 12, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cancleMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cancleMouseClicked
       add_edit_cus mainaddcus = new add_edit_cus();
        mainaddcus.show();  
        this.dispose();
    }//GEN-LAST:event_cancleMouseClicked

    private void listallemMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listallemMouseClicked
        int index = listallem.getSelectedRow();
        this.username = (String) listallem.getValueAt(index, 2);
    }//GEN-LAST:event_listallemMouseClicked

    private void okMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_okMouseClicked
        if("".equals(username)){
            JOptionPane.showMessageDialog(this,"กรุณาเลือกรายการก่อนค่ะ","เลือกรายการ",JOptionPane.PLAIN_MESSAGE);
        }
        else{
            add_edit_cus mainaddcus = new add_edit_cus();
            mainaddcus.editemployee(username);
            mainaddcus.show();        
            this.dispose(); 
        }
        
    }//GEN-LAST:event_okMouseClicked

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new edit_em().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancle;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable listallem;
    private javax.swing.JButton ok;
    // End of variables declaration//GEN-END:variables

    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/img/Cash-register-icon.png")));
    }
    
    public final void printem(){
        modellistgroup = (DefaultTableModel) listallem.getModel();
        try{
            int totalrow = listallem.getRowCount()-1;
            while(totalrow > -1){
                modellistgroup.removeRow(totalrow);
                totalrow --;
            }
            String listallcus = "SELECT employee.fname,employee.lname,employee.user,user.usergroup FROM  employee INNER JOIN user ON employee.user = user.user";
            ResultSet rs = conn.createStatement().executeQuery(listallcus);
            int row = 0;
            while(rs.next()){
                modellistgroup.addRow(new Object[0]);
                modellistgroup.setValueAt(row+1, row, 0);
                modellistgroup.setValueAt(rs.getString("fname")+"  "+rs.getString("lname"), row, 1);
                modellistgroup.setValueAt(rs.getString("user"), row, 2);
                modellistgroup.setValueAt(rs.getString("usergroup"), row, 3);
                row++;
            }
            listallem.setModel(modellistgroup);
        }
        catch(SQLException e){
            JOptionPane.showMessageDialog(this,e,"ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
        }
    }
    
}
