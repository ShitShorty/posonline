package posonline;

import config.dbconnect;
import java.awt.Component;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.TimeZone;
import javax.swing.JOptionPane;

public final class addproduct extends javax.swing.JFrame {
    
    Connection conn = new dbconnect().getconnection();
    int idgroupprod = 0;
    Date date = new Date();
    java.text.SimpleDateFormat df= new java.text.SimpleDateFormat();
    boolean checkid = false;
    int countold = 0;

    public addproduct() {
        initComponents();
        setIcon();
        printlistbox();
        barcode.requestFocus();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        exit = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        barcode = new javax.swing.JTextField();
        nameprod = new javax.swing.JTextField();
        grouppord = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        dateimport = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        bathtoon = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        bathsale = new javax.swing.JTextField();
        saveadd = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        countprod = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        namecount = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        detail = new javax.swing.JTextArea();
        jLabel12 = new javax.swing.JLabel();
        importfrom = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        barcode2 = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        detail2 = new javax.swing.JTextArea();
        jLabel15 = new javax.swing.JLabel();
        count2 = new javax.swing.JTextField();
        save = new javax.swing.JButton();
        clrar = new javax.swing.JButton();
        jLabel16 = new javax.swing.JLabel();
        dateimport2 = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        barcode3 = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        detail3 = new javax.swing.JTextArea();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("เพิ่ม / ลด / แก้ไข สินค้า");
        setBounds(new java.awt.Rectangle(400, 0, 739, 671));
        setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        setMaximizedBounds(new java.awt.Rectangle(400, 0, 739, 671));
        setMinimumSize(new java.awt.Dimension(739, 671));

        exit.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        exit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Apps-session-logout-icon.png"))); // NOI18N
        exit.setText("ออก");
        exit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                exitMouseClicked(evt);
            }
        });

        jTabbedPane1.setTabPlacement(javax.swing.JTabbedPane.LEFT);
        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jLabel1.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel1.setText("รหัสสินค้า*");

        jLabel2.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel2.setText("ชื่อสินค้า*");

        jLabel3.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel3.setText("หมวดสินค้า*");

        barcode.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        barcode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                barcodeKeyReleased(evt);
            }
            public void keyPressed(java.awt.event.KeyEvent evt) {
                barcodeKeyPressed(evt);
            }
        });

        nameprod.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        nameprod.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                nameprodKeyPressed(evt);
            }
        });

        grouppord.setFont(new java.awt.Font("AngsanaUPC", 1, 18)); // NOI18N
        grouppord.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "กรุณาเลือกหมวดสินค้า" }));
        grouppord.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                grouppordItemStateChanged(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel4.setText("วันที่นำเข้า*");

        dateimport.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        dateimport.setEnabled(false);

        jLabel5.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel5.setText("ราคาทุน*");

        bathtoon.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        bathtoon.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bathtoonMouseClicked(evt);
            }
        });
        bathtoon.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                bathtoonKeyPressed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel6.setText("บาท");

        jLabel7.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel7.setText("ราคาขาย*");

        jLabel8.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel8.setText("บาท");

        bathsale.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        bathsale.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bathsaleMouseClicked(evt);
            }
        });
        bathsale.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                bathsaleKeyPressed(evt);
            }
        });

        saveadd.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        saveadd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Floppy-Small-icon.png"))); // NOI18N
        saveadd.setText("บันทึก");
        saveadd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                saveaddMouseClicked(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Actions-edit-clear-icon.png"))); // NOI18N
        jButton2.setText("ล้างข้อมูล");
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton2MouseClicked(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel9.setText("จำนวน*");

        countprod.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        countprod.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                countprodMouseClicked(evt);
            }
        });
        countprod.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                countprodKeyPressed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel10.setText("หน่วยสินค้า*");

        namecount.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        namecount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                namecountKeyPressed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel11.setText("รายละเอียดเพิ่มเติม");

        detail.setColumns(20);
        detail.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        detail.setRows(5);
        jScrollPane1.setViewportView(detail);

        jLabel12.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel12.setText("แหล่งที่มาสินค้า");

        importfrom.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        importfrom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                importfromKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11)
                            .addComponent(jLabel12))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE)
                            .addComponent(importfrom)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(grouppord, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(44, 44, 44)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel5)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel1))
                                .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.LEADING))
                            .addGap(18, 18, 18)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(barcode, javax.swing.GroupLayout.PREFERRED_SIZE, 436, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(nameprod, javax.swing.GroupLayout.PREFERRED_SIZE, 436, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(countprod, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGap(13, 13, 13))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addComponent(jLabel10)
                                            .addGap(18, 18, 18)
                                            .addComponent(namecount, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addComponent(bathtoon, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(18, 18, 18)
                                            .addComponent(jLabel6)
                                            .addGap(27, 27, 27)
                                            .addComponent(jLabel7)
                                            .addGap(36, 36, 36)
                                            .addComponent(bathsale, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(dateimport, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel8))))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(saveadd)
                            .addGap(18, 18, 18)
                            .addComponent(jButton2)
                            .addGap(39, 39, 39))))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(barcode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nameprod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(grouppord, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(dateimport, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bathtoon, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(bathsale, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(namecount, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(countprod, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(23, 23, 23)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(importfrom, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(saveadd)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(65, 65, 65))
        );

        jTabbedPane1.addTab("เพิ่ม/แก้ไข สินค้าใน Stock", jPanel1);

        jLabel13.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel13.setText("รหัสสินค้า");

        barcode2.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        barcode2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                barcode2KeyReleased(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel14.setText("รายละเอียดสินค้า");

        detail2.setColumns(20);
        detail2.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        detail2.setRows(5);
        jScrollPane2.setViewportView(detail2);

        jLabel15.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel15.setText("จำนวนที่นำเข้า");

        count2.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        count2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                count2KeyReleased(evt);
            }
        });

        save.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        save.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Floppy-Small-icon.png"))); // NOI18N
        save.setText("เพิ่ม");
        save.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                saveMouseClicked(evt);
            }
        });

        clrar.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        clrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Actions-edit-clear-icon.png"))); // NOI18N
        clrar.setText("ล้างข้อมูล");
        clrar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                clrarMouseClicked(evt);
            }
        });

        jLabel16.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel16.setText("วันที่นำเข้า");

        dateimport2.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        dateimport2.setEnabled(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel14)
                    .addComponent(jLabel15)
                    .addComponent(jLabel13)
                    .addComponent(jLabel16))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(save)
                        .addGap(18, 18, 18)
                        .addComponent(clrar))
                    .addComponent(barcode2, javax.swing.GroupLayout.PREFERRED_SIZE, 382, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 382, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addComponent(count2, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(0, 0, Short.MAX_VALUE)))
                    .addComponent(dateimport2, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(48, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(barcode2))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dateimport2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(count2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(save)
                    .addComponent(clrar, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(88, 88, 88))
        );

        jTabbedPane1.addTab("นำเข้าสินค้าใน Stock", jPanel2);

        jLabel17.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel17.setText("รหัสสินค้า");

        barcode3.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        barcode3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                barcode3KeyReleased(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jLabel18.setText("รายละเอียดสินค้า");

        detail3.setColumns(20);
        detail3.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        detail3.setRows(5);
        jScrollPane3.setViewportView(detail3);

        jButton3.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cancel-icon.png"))); // NOI18N
        jButton3.setText("ลบสินค้า");
        jButton3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton3MouseClicked(evt);
            }
        });

        jButton4.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Actions-edit-clear-icon.png"))); // NOI18N
        jButton4.setText("ล้างข้อมูล");
        jButton4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton4MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel17)
                    .addComponent(jLabel18))
                .addGap(21, 21, 21)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
                        .addComponent(jButton4))
                    .addComponent(barcode3)
                    .addComponent(jScrollPane3))
                .addContainerGap(81, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(barcode3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 362, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(59, 59, 59))
        );

        jTabbedPane1.addTab("ลดสินค้าออกจาก Stock", jPanel3);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(exit))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 781, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 534, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(exit)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exitMouseClicked
        this.dispose();
    }//GEN-LAST:event_exitMouseClicked

    private void saveaddMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveaddMouseClicked
        insertdata();
    }//GEN-LAST:event_saveaddMouseClicked

    private void grouppordItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_grouppordItemStateChanged
        if(grouppord.getSelectedItem() == "กรุณาเลือกหมวดสินค้า"){
            idgroupprod = 0;
        }
        else{
            int num = grouppord.getSelectedItem().toString().indexOf("(")+1;
            int num2 = grouppord.getSelectedItem().toString().indexOf(")");
            String outnum = grouppord.getSelectedItem().toString().substring(num,num2);
            idgroupprod = Integer.parseInt(outnum);
        }
    }//GEN-LAST:event_grouppordItemStateChanged

    private void barcodeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_barcodeKeyPressed
        if(evt.getKeyCode() == 10){
           insertdata();
        } 
    }//GEN-LAST:event_barcodeKeyPressed

    private void nameprodKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nameprodKeyPressed
        if(evt.getKeyCode() == 10){
           insertdata();
        } 
    }//GEN-LAST:event_nameprodKeyPressed

    private void bathtoonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bathtoonKeyPressed
        if(evt.getKeyCode() == 10){
           insertdata();
        } 
    }//GEN-LAST:event_bathtoonKeyPressed

    private void bathsaleKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bathsaleKeyPressed
        if(evt.getKeyCode() == 10){
           insertdata();
        } 
    }//GEN-LAST:event_bathsaleKeyPressed

    private void countprodKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_countprodKeyPressed
        if(evt.getKeyCode() == 10){
           insertdata();
        } 
    }//GEN-LAST:event_countprodKeyPressed

    private void namecountKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_namecountKeyPressed
        if(evt.getKeyCode() == 10){
           insertdata();
        } 
    }//GEN-LAST:event_namecountKeyPressed

    private void importfromKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_importfromKeyPressed
        if(evt.getKeyCode() == 10){
           insertdata();
        } 
    }//GEN-LAST:event_importfromKeyPressed

    private void bathtoonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bathtoonMouseClicked
        bathtoon.requestFocus();
        bathtoon.selectAll();
    }//GEN-LAST:event_bathtoonMouseClicked

    private void bathsaleMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bathsaleMouseClicked
        bathsale.requestFocus();
        bathsale.selectAll();
    }//GEN-LAST:event_bathsaleMouseClicked

    private void jButton2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MouseClicked
        cleartext();
    }//GEN-LAST:event_jButton2MouseClicked

    private void countprodMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_countprodMouseClicked
        countprod.requestFocus();
        countprod.selectAll();
    }//GEN-LAST:event_countprodMouseClicked

    private void barcodeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_barcodeKeyReleased
        try{    
                String sqlsearch = "SELECT * FROM product INNER JOIN namegroupprod ON product.group_product = namegroupprod.id";
                ResultSet out = conn.createStatement().executeQuery(sqlsearch);
                String textcheck = barcode.getText();
                while(out.next()){
                    if(textcheck.equals(out.getString("id_product"))){
                        nameprod.setText(out.getString("name_product"));
                        countprod.setText(out.getString("sumcount"));
                        bathtoon.setText(out.getString("pricetoon"));
                        bathsale.setText(out.getString("pricesell"));
                        namecount.setText(out.getString("unit_product"));
                        importfrom.setText(out.getString("importfrom"));
                        detail.setText(out.getString("detail"));
                        saveadd.setText("แก้ไข");
                        barcode.disable();
                        grouppord.setSelectedItem(""+out.getString("name")+"("+out.getString("group_product")+")");
                        idgroupprod = Integer.parseInt(out.getString("group_product"));
                        dateimport.setText("");
                        countprod.disable();
                        checkid = true;
                        break;
                    }
                }
        }
        catch(SQLException e){
            JOptionPane.showMessageDialog(this,e,"ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_barcodeKeyReleased

    private void clrarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_clrarMouseClicked
        cleartext2();
    }//GEN-LAST:event_clrarMouseClicked

    private void barcode2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_barcode2KeyReleased
        String sql2 = "SELECT * FROM product INNER JOIN namegroupprod ON product.group_product = namegroupprod.id";
        try {
            ResultSet out = conn.createStatement().executeQuery(sql2);
            String textcheck = barcode2.getText();
            while(out.next()){
                if(textcheck.equals(out.getString("id_product"))){
                    detail2.setText("รหัสสินค้า:\t"+out.getString("id_product")+"\n"
                                    +"ชื่อสินค้า:\t\t"+out.getString("name_product")+"\n"
                                    +"กลุ่มสินค้า:\t\t"+out.getString("name")+"\n"
                                    +"ราคาทุน:\t\t"+out.getString("pricetoon")+" บาท "+"\n"
                                    +"ราคาขาย:\t\t"+out.getString("pricesell")+" บาท "+"\n"
                                    +"จำนวนคงเหลือ:\t"+out.getString("sumcount") + " " + out.getString("unit_product")+"\n"
                                    );
                    countold = out.getInt("sumcount");
                    detail2.disable();
                    barcode2.disable();
                    count2.requestFocus();
                    break;
                }
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this,e,"ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_barcode2KeyReleased

    private void saveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveMouseClicked
        saveaddproduct();
    }//GEN-LAST:event_saveMouseClicked

    private void count2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_count2KeyReleased
        if(evt.getKeyCode() == 10){
           saveaddproduct();
        } 
    }//GEN-LAST:event_count2KeyReleased

    private void barcode3KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_barcode3KeyReleased
        String sql2 = "SELECT * FROM product INNER JOIN namegroupprod ON product.group_product = namegroupprod.id";
        try {
            ResultSet out = conn.createStatement().executeQuery(sql2);
            String textcheck = barcode3.getText();
            while(out.next()){
                if(textcheck.equals(out.getString("id_product"))){
                    detail3.setText("รหัสสินค้า:\t"+out.getString("id_product")+"\n"
                                    +"ชื่อสินค้า:\t\t"+out.getString("name_product")+"\n"
                                    +"กลุ่มสินค้า:\t\t"+out.getString("name")+"\n"
                                    +"ราคาทุน:\t\t"+out.getString("pricetoon")+" บาท "+"\n"
                                    +"ราคาขาย:\t\t"+out.getString("pricesell")+" บาท "+"\n"
                                    +"จำนวนคงเหลือ:\t"+out.getString("sumcount") + " " + out.getString("unit_product")+"\n"
                                    );
                    countold = out.getInt("sumcount");
                    detail3.disable();
                    barcode3.disable();
                    break;
                }
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this,e,"ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_barcode3KeyReleased

    private void jButton4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton4MouseClicked
        cleartext3();
    }//GEN-LAST:event_jButton4MouseClicked

    private void jButton3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton3MouseClicked
        try {
            String sqldelete = "DELETE FROM product WHERE product.id_product = ?";
            PreparedStatement predelete = conn.prepareStatement(sqldelete);
            Object[] options = {"ใช่",
                    "ไม่"};
            Component frame = null;
            int n = JOptionPane.showOptionDialog(frame,
                "คุณแน่ใจ หรือ ไม่ ที่จะลบข้อมูลนี้ ออกจากฐานข้อมูล",
                "คุณมั่นใจหรือไม่",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,     //do not use a custom Icon
                options,  //the titles of buttons
                options[0]); //default button title
            if(n == 0){
                predelete.setString(1, barcode3.getText());
                if(predelete.executeUpdate() != -1){
                    JOptionPane.showMessageDialog(this,"ลบข้อมูลเรียบร้อยแล้วค่ะ","ลบข้อมูล",JOptionPane.PLAIN_MESSAGE);
                    cleartext3();
                }
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this,e,"ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton3MouseClicked
    
    public void saveaddproduct(){
        if("".equals(barcode2.getText())){
            JOptionPane.showMessageDialog(this,"กรุณาระบุรหัสบาร์โค้ดด้วยค่ะ","กรอกข้อมูล",JOptionPane.ERROR_MESSAGE);
            barcode2.requestFocus();
        }
        else if("".equals(count2.getText())){
            JOptionPane.showMessageDialog(this,"กรุณากรอกจำนวนสินค้าที่จะเพิ่มด้วยค่ะ","กรอกข้อมูล",JOptionPane.ERROR_MESSAGE);
            count2.requestFocus();
        }
        else{
            String sqlupdate = "UPDATE product SET sumcount = ? WHERE id_product = ?";
            try {
                PreparedStatement preupdate = conn.prepareStatement(sqlupdate);
                int count = Integer.parseInt(count2.getText())+countold; 
                preupdate.setInt(1, count);
                preupdate.setString(2, barcode2.getText());

                String sql2 = "INSERT INTO dateimport(id,id_product,count_product,date_import) VALUES(NULL ,?,?,?);";
                PreparedStatement pre2 = conn.prepareStatement(sql2);
                pre2.setString(1, barcode2.getText());
                Integer editcountprod = Integer.parseInt(count2.getText());
                pre2.setInt(2, editcountprod);
                df.applyPattern("yyyy-MM-dd");
                df.setTimeZone(TimeZone.getTimeZone("GMT+7"));
                pre2.setString(3, df.format(date));

                if(preupdate.executeUpdate() != -1 && pre2.executeUpdate() != -1){
                    JOptionPane.showMessageDialog(this,"เพิ่มจำนวนสินค้าเรียบร้อยแล้วค่ะ","แก้ไขข้อมูล",JOptionPane.PLAIN_MESSAGE);
                    cleartext2();
                }
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(this,e,"ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    public void cleartext3(){
        barcode3.setText("");
        detail3.setText("");
        barcode3.enable();
        detail3.enable();
        barcode3.requestFocus();
    }
    
    public void cleartext2(){
        barcode2.setText("");
        detail2.setText("");
        dateimport2.setText("");
        count2.setText("");
        barcode2.enable();
        detail2.enable();
        barcode2.requestFocus();
        countold = 0;
        df.applyPattern("yyyy-MM-dd");
        df.setTimeZone(TimeZone.getTimeZone("GMT+7"));
        dateimport2.setText(df.format(date));
    }
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new addproduct().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField barcode;
    private javax.swing.JTextField barcode2;
    private javax.swing.JTextField barcode3;
    private javax.swing.JTextField bathsale;
    private javax.swing.JTextField bathtoon;
    private javax.swing.JButton clrar;
    private javax.swing.JTextField count2;
    private javax.swing.JTextField countprod;
    private javax.swing.JTextField dateimport;
    private javax.swing.JTextField dateimport2;
    private javax.swing.JTextArea detail;
    private javax.swing.JTextArea detail2;
    private javax.swing.JTextArea detail3;
    private javax.swing.JButton exit;
    private javax.swing.JComboBox grouppord;
    private javax.swing.JTextField importfrom;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextField namecount;
    private javax.swing.JTextField nameprod;
    private javax.swing.JButton save;
    private javax.swing.JButton saveadd;
    // End of variables declaration//GEN-END:variables

    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/img/Cash-register-icon.png")));
    }
    public void cleartext(){
        barcode.setText(null);
        nameprod.setText(null);
        grouppord.setSelectedIndex(0);
        bathtoon.setText(null);
        bathsale.setText(null);
        countprod.setText(null);
        namecount.setText(null);
        importfrom.setText(null);
        detail.setText(null);
        checkid = false;
        barcode.requestFocus();
        countprod.enable();
        saveadd.setText("บันทึก");
        barcode.enable();
        df.applyPattern("yyyy-MM-dd");
        df.setTimeZone(TimeZone.getTimeZone("GMT+7"));
        dateimport.setText(df.format(date));
        dateimport2.setText(df.format(date));
    }
    public void printlistbox(){
        try{
            df.applyPattern("yyyy-MM-dd");
            df.setTimeZone(TimeZone.getTimeZone("GMT+7"));
            dateimport.setText(df.format(date));
            dateimport2.setText(df.format(date));
            String sql = "SELECT * FROM namegroupprod";
            ResultSet rs = conn.createStatement().executeQuery(sql);
            while(rs.next()){
                grouppord.addItem(rs.getString("name")+"("+rs.getString("id")+")");
            }
        }
        catch(SQLException e){
            JOptionPane.showMessageDialog(this,e,"ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
        }
    }

    private void insertdata(){
            try{
                if(checkid == true){
                    String sqlupdate = "UPDATE product "
                            + "SET id_product = ? , "
                            + "name_product = ? , "
                            + "group_product = ? , "
                            + "pricetoon = ? , "
                            + "pricesell = ? , "
                            + "unit_product = ? , "
                            + "importfrom = ? , "
                            + "detail = ? "
                            + "WHERE id_product = ?";
                    PreparedStatement preupdate = conn.prepareStatement(sqlupdate);
                    preupdate.setString(1, barcode.getText());
                    preupdate.setString(2, nameprod.getText());
                    preupdate.setInt(3, idgroupprod);
                    double editbathtoon = Double.parseDouble(bathtoon.getText());
                    double editbathsell = Double.parseDouble(bathsale.getText());
                    preupdate.setDouble(4, editbathtoon);
                    preupdate.setDouble(5, editbathsell);
                    preupdate.setString(6, namecount.getText());
                    preupdate.setString(7, importfrom.getText());
                    preupdate.setString(8, detail.getText());
                    preupdate.setString(9, barcode.getText());
                    if(preupdate.executeUpdate() != -1){
                        JOptionPane.showMessageDialog(this,"แก้ไขข้อมูลเสร็จสิ้น","แก้ไขข้อมูล",JOptionPane.PLAIN_MESSAGE);
                        cleartext();
                    }
                }
                else if("".equals(barcode.getText())){
                    JOptionPane.showMessageDialog(this,"กรุณากรอกรหัสสินค้าค่ะ","กรอกข้อมูลไม่ครบ",JOptionPane.ERROR_MESSAGE);
                    barcode.requestFocus();
                }
                else if("".equals(nameprod.getText())){
                    JOptionPane.showMessageDialog(this,"กรุณากรอกชื่อสินค้าค่ะ","กรอกข้อมูลไม่ครบ",JOptionPane.ERROR_MESSAGE);
                    nameprod.requestFocus();
                }
                else if(idgroupprod == 0){
                    JOptionPane.showMessageDialog(this,"กรุณาเลือกหมวดสินค้าค่ะ","กรอกข้อมูลไม่ครบ",JOptionPane.ERROR_MESSAGE);
                    grouppord.requestFocus();
                }
                else if("".equals(bathtoon.getText())){
                    JOptionPane.showMessageDialog(this,"กรุณากรอกราคาทุนค่ะ","กรอกข้อมูลไม่ครบ",JOptionPane.ERROR_MESSAGE);
                    bathtoon.requestFocus();
                }
                else if("".equals(bathsale.getText())){
                    JOptionPane.showMessageDialog(this,"กรุณากรอกราคาขายค่ะ","กรอกข้อมูลไม่ครบ",JOptionPane.ERROR_MESSAGE);
                    bathsale.requestFocus();
                }
                else if("".equals(countprod.getText())){
                    JOptionPane.showMessageDialog(this,"กรุณากรอกจำนวนค่ะ","กรอกข้อมูลไม่ครบ",JOptionPane.ERROR_MESSAGE);
                    countprod.requestFocus();
                }
                else if("".equals(namecount.getText())){
                    JOptionPane.showMessageDialog(this,"กรุณากรอกหน่วยสินค้าค่ะ","กรอกข้อมูลไม่ครบ",JOptionPane.ERROR_MESSAGE);
                    namecount.requestFocus();
                }
                else{
                    String sql2 = "INSERT INTO dateimport(id , id_product , count_product , date_import)" 
                            + "VALUES(NULL ,?,?,?);";
                    PreparedStatement pre2 = conn.prepareStatement(sql2);
                    pre2.setString(1, barcode.getText());
                    Integer editcountprod = Integer.parseInt(countprod.getText());
                    pre2.setInt(2, editcountprod);
                    df.applyPattern("yyyy-MM-dd");
                    df.setTimeZone(TimeZone.getTimeZone("GMT+7"));
                    pre2.setString(3, df.format(date));

                    String sql = "INSERT INTO product"
                            + "(id , id_product , name_product , group_product , pricetoon , pricesell , unit_product , importfrom , detail , sumcount)"
                            + "VALUES (NULL,?,?,?,?,?,?,?,?,?);";
                    PreparedStatement pre = conn.prepareStatement(sql);
                    pre.setString(1, barcode.getText());
                    pre.setString(2, nameprod.getText());
                    pre.setInt(3, idgroupprod);
                    double editbathtoon = Double.parseDouble(bathtoon.getText());
                    double editbathsell = Double.parseDouble(bathsale.getText());
                    pre.setDouble(4, editbathtoon);
                    pre.setDouble(5, editbathsell);
                    pre.setString(6, namecount.getText());
                    pre.setString(7, importfrom.getText());
                    pre.setString(8, detail.getText());
                    pre.setInt(9, editcountprod);
                    if(pre.executeUpdate() != -1 && pre2.executeUpdate() != -1){
                        JOptionPane.showMessageDialog(this,"บันทึกข้อมูลเสร็จสิ้น","บันทึกข้อมูล",JOptionPane.PLAIN_MESSAGE);
                        cleartext();
                    }
                }
        }
        catch(SQLException e){
            JOptionPane.showMessageDialog(this,e,"ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
        }
        catch(NumberFormatException e){
            JOptionPane.showMessageDialog(this,"จำนวน ราคาทุน และ ราคาขาย ต้องเป็นตัวเลขเท่านั้น ค่ะ","ตรวจพบข้อผิดพลาด",JOptionPane.ERROR_MESSAGE);
        }
    }
}
