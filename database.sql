-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- โฮสต์: localhost
-- เวลาในการสร้าง: 
-- รุ่นของเซิร์ฟเวอร์: 5.0.51
-- รุ่นของ PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- ฐานข้อมูล: `posonline`
-- 

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `dateimport`
-- 

CREATE TABLE `dateimport` (
  `id` int(11) NOT NULL auto_increment,
  `id_product` varchar(50) collate utf8_unicode_ci NOT NULL,
  `count_product` int(11) NOT NULL,
  `date_import` varchar(50) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

-- 
-- dump ตาราง `dateimport`
-- 

INSERT INTO `dateimport` VALUES (20, 'c', 50, '2013-11-13');
INSERT INTO `dateimport` VALUES (19, 'b', 50, '2013-11-12');
INSERT INTO `dateimport` VALUES (18, 'a', 15, '2013-11-04');
INSERT INTO `dateimport` VALUES (17, 'a', 30, '2013-11-04');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `employee`
-- 

CREATE TABLE `employee` (
  `id_em` int(11) NOT NULL auto_increment,
  `fname` varchar(50) collate utf8_unicode_ci NOT NULL,
  `lname` varchar(50) collate utf8_unicode_ci NOT NULL,
  `sex` varchar(5) collate utf8_unicode_ci NOT NULL,
  `birthday` varchar(50) collate utf8_unicode_ci NOT NULL,
  `em_address` varchar(250) collate utf8_unicode_ci NOT NULL,
  `friend_address` varchar(250) collate utf8_unicode_ci NOT NULL,
  `tel_em` int(13) NOT NULL,
  `tel_friend` int(13) NOT NULL,
  `other` varchar(250) collate utf8_unicode_ci NOT NULL,
  `num_em` double NOT NULL,
  `user` varchar(50) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id_em`,`num_em`,`user`),
  UNIQUE KEY `user` (`user`),
  UNIQUE KEY `num_em` (`num_em`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=22 ;

-- 
-- dump ตาราง `employee`
-- 

INSERT INTO `employee` VALUES (15, 'ธนพล', 'โสดาวิชิต', 'ชาย', '1990-6-10', '420 / 2 หมู่ 5 ต.สุรนารี อ.เมือง จ.นครราชสีมา 30000', 'นางสาวเกวลิน พงษ์พัฒน 420/2 หมู่ 5 ต.สุรนารี อ.เมือง จ.นครราชสีมา 30000', 877740516, 877725218, 'ทดสอบการบันทึก และ แก้ไข้ จ้า', 1419900230161, 'realsagi');
INSERT INTO `employee` VALUES (19, 'จินตนาการ', 'เดาเอา', 'ชาย', '1902-1-1', 'admin', 'admin', 877725218, 877725218, 'admin กากๆ', 1430700055937, 'admin');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `namegroupprod`
-- 

CREATE TABLE `namegroupprod` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name_2` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=131 ;

-- 
-- dump ตาราง `namegroupprod`
-- 

INSERT INTO `namegroupprod` VALUES (128, 'รองเท้า');
INSERT INTO `namegroupprod` VALUES (130, 'ผ้าพันคอ');
INSERT INTO `namegroupprod` VALUES (126, 'หมวก');
INSERT INTO `namegroupprod` VALUES (127, 'กระเป๋า');
INSERT INTO `namegroupprod` VALUES (125, 'กางเกง');
INSERT INTO `namegroupprod` VALUES (123, 'เสื้อ');

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `product`
-- 

CREATE TABLE `product` (
  `id` int(11) NOT NULL auto_increment,
  `id_product` varchar(50) collate utf8_unicode_ci NOT NULL,
  `name_product` varchar(50) collate utf8_unicode_ci NOT NULL,
  `group_product` int(11) NOT NULL,
  `pricetoon` double NOT NULL,
  `pricesell` double NOT NULL,
  `unit_product` varchar(50) collate utf8_unicode_ci NOT NULL,
  `importfrom` varchar(200) collate utf8_unicode_ci default NULL,
  `detail` varchar(250) collate utf8_unicode_ci default NULL,
  `sumcount` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `id_product` (`id_product`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=23 ;

-- 
-- dump ตาราง `product`
-- 

INSERT INTO `product` VALUES (21, 'b', 'กางเกงในเด็ก', 125, 150, 200, 'ตัว', 'เจ๊สมร', 'กางเกงลิง สตรี มีคัน', 50);
INSERT INTO `product` VALUES (22, 'c', 'เสื้อคอยืด', 123, 150, 350, 'ตัว', 'แม่ใหญ่เล้ง', 'เสื้อใส่แล้วคอยืดยาว', 50);
INSERT INTO `product` VALUES (20, 'a', 'หมวกแก๊บเด็ก', 126, 120, 320, 'ใบ', 'ยายเล้งหน้าวัดโพธิ์', 'หมวกเปิดตรงกลางหัว\n', 45);

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `user`
-- 

CREATE TABLE `user` (
  `id` int(11) NOT NULL auto_increment,
  `user` varchar(50) collate utf8_unicode_ci NOT NULL,
  `pass` varchar(50) collate utf8_unicode_ci NOT NULL,
  `usergroup` varchar(50) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `user` (`user`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

-- 
-- dump ตาราง `user`
-- 

INSERT INTO `user` VALUES (11, 'admin', 'admin', 'admin');
INSERT INTO `user` VALUES (7, 'realsagi', '7412374123', 'cashier');
